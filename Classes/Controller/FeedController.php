<?php

namespace VKAC\RssReader\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use Vinelab\Rss\Rss;

class FeedController extends ActionController
{
    public function showAction()
    {
        $feedUrl = $this->settings['feedUrl'];
        $rss = new Rss();
        $feed = $rss->feed($feedUrl);

        $this->view->assign('articles', $feed->articles());
    }
}
