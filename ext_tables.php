<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'VKAC.' . $_EXTKEY,
    'Show',
    'RSS feed reader'
);

$pluginSignature = str_replace('_', '', $_EXTKEY) . '_show';

$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'code,layout,select_key,pages,recursive';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_feed.xml'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    $_EXTKEY,
    'Configuration/TypoScript',
    'Google Maps API Extbase'
);


