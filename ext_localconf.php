<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'VKAC.' . $_EXTKEY,
    'RssReader',
    [
        'Feed' => 'show'
    ],
    [
        'Feed' => 'show'
    ]
);
