<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'RSS reader',
    'description' => 'Print an RSS feed',
    'category' => 'plugin',
    'author' => 'Christian Spoo',
    'author_company' => '',
    'author_email' => 'mail@christian-spoo.info',
    'dependencies' => 'extbase,fluid',
    'state' => 'alpha',
    'clearCacheOnLoad' => '1',
    'version' => '0.0.0',
    'constraints' => [
        'depends' => [
            'php' => '7.0.0-7.99.99',
            'typo3' => '7.6.7-8.99.99',
            'extbase' => '1.0.0-0.0.0',
            'fluid' => '1.0.0-0.0.0'
        ]
    ],
    'autoload' => [
        'psr-4' => [
            'VKAC\\RssReader\\' => 'Classes'
        ]
    ]
];
